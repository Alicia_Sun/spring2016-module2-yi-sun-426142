	<!DOCTYPE html>
	<head>
	<meta charset="utf-8"/>
	<title>Calculator</title>
	<style type="text/css">
	body{
		width: 760px; /* how wide to make your web page */
		background-color: teal; /* what color to make the background */
		margin: 0 auto;
		padding: 0;
		font:12px/16px Verdana, sans-serif; /* default font */
	}
	div#main{
		background-color: #FFF;
		margin: 0;
		padding: 10px;
	}
	</style>
	</head>
	<body><div id="main">
	 
	<!-- CONTENT HERE -->
	
	<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
		<label>Number 1:<input type="number" step="any" name="n1"></label>
		<label>Number 2:<input type="number" step="any" name="n2"></label><br>
		<input type="radio" name="operation" value="add" checked>Add<br>
		<input type="radio" name="operation" value="substract" checked>Substract<br>
		<input type="radio" name="operation" value="multiply" checked>Multiply<br>
		<input type="radio" name="operation" value="divide" checked>Divide<br>
		<!--<label for="result">Result:</label>-->
		<input type="submit" value="Go"  /><br>
		
	</form>
	
	<?php
	if(isset($_POST['n1'])&&isset($_POST['n2'])&&isset($_POST['operation'])){
		$a=$_POST['n1'];
		$b=$_POST['n2'];
		$op=$_POST['operation'];
		switch($op){
			case "add":
				$result = $a+$b;
				break;
			case "substract":
				$result = $a-$b;
				break;
			case "multiply":
				$result = $a*$b;
				break;
			case "divide":
				if($b!=0){
				$result = $a/$b;
				}
				break;
			default:
				$result = $a+$b;
				break;
		}
		if($b!=0){
			printf("<p>Your result is %g!</p>\n", htmlentities($result));
		}
		else{
			printf("<p>Seriously?!Divided by zero?!</p>");
		}
	}
	?>
	 
	</div></body>
	</html>